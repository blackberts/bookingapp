1. Creating Data base schemas and EF models for authorization & authentication + migration
2. Creating jwt factory and controller for authorization & authentication 
3. Creating CRUD controller for user management
4. Creating application initial styles and front-end for authorization & authentication & user manipulation  controllers
5. Updating Data base with booking models (room / hotel, pricing, booking status etc.) & creating booking controller
6. Creating front-end for booking  controller
7. Adding docker support
